# Tugas 1 PPW Kelompok 9

## Anggota Kelompok
1. Naufaldi Athallah Rifqi 1706023782
2. Azhar Difa Arnanda 1706039540
3. Dimas Aditya Faiz 1706039686
4. Faishal Ridwan 1706040201

## Status Aplikasi
[![Pipeline](https://gitlab.com/kelompok1-tugas1-ppwc/TUGAS1_PPW/badges/master/pipeline.svg)](https://gitlab.com/kelompok1-tugas1-ppwc/TUGAS1_PPW/commits/master)
[![Coverage](https://gitlab.com/kelompok1-tugas1-ppwc/TUGAS1_PPW/badges/master/coverage.svg)](https://gitlab.com/kelompok1-tugas1-ppwc/TUGAS1_PPW/commits/master)

## Link Heroku App
https://kelompok1-tugas1-ppw.herokuapp.com/